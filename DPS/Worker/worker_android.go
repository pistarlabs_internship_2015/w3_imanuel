//BEANSTALKD ANDROID CONSUMER

package main
import (
    "github.com/iwanbk/gobeanstalk"
    "log"
	"strings"
    "fmt"
    "github.com/googollee/go-gcm"
    "io/ioutil" 
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
)

type Applications struct {
    ID bson.ObjectId `bson:"_id,omitempty"`
    Name string `bson:"name"`
    App_Version string `bson:"app_version"`
    Key_Access string `bson:"key_access"`
    GCM_Access_Key string `bson:"gcm_access_key"`
    Baidu_Access_Key string `bson:"baidu_access_key"`
    APNS_PEM string `bson:"apns_pem"`
    APNS_PEM_NoEnc string `bson:"apns_pem_noenc"`
}

func main() {
    conn, err := gobeanstalk.Dial("192.168.0.45:11300")
    if err != nil {
        log.Fatal(err)
    }
    for {
        conn.Watch("gcm")
		j, err := conn.Reserve()
        if err != nil {
            log.Fatal(err)
        }
        log.Printf("id:%d, body:%s\n", j.ID, string(j.Body))
		
		//SPLIT DATA
		result := strings.Split(string(j.Body), "#####CONCAT#####")
		// Display all elements.
		for i := range result {
			fmt.Println("result #",i, result[i])
		}
		// Length
		fmt.Println(len(result))


        //GET GCM ACCESS KEY 
        gcm_key_dir:=FindGCMKey()
        GCM_ACCESS_KEY,_ := ioutil.ReadFile(gcm_key_dir)
    	
		//PUSH NOTIFICATION PROCESS//
		pushToAndroid(string(GCM_ACCESS_KEY),result[0],result[2])
		
	
        err = conn.Delete(j.ID) 
        if err != nil {
            log.Fatal(err)
        }
    }
}

func pushToAndroid(app_key string,device_token string,Push_Code string){
    client := gcm.New(app_key)

    load := gcm.NewMessage(device_token)
    load.AddRecipient("abc")
    load.SetPayload("data", "1")
    load.CollapseKey = "demo"
    load.DelayWhileIdle = true
    load.TimeToLive = 10

    resp, err := client.Send(load)

    fmt.Printf("id: %+v\n", resp)
    fmt.Println("err:", err)
    fmt.Println("err index:", resp.ErrorIndexes())
    fmt.Println("reg index:", resp.RefreshIndexes())

    if(err==nil){
        fmt.Println("andro push : success")
        ChangeStatus(Push_Code)
    }else{
        fmt.Println("andro push : failed")
    }
}

func FindGCMKey() (string){
    session, err := mgo.Dial("localhost:27017")
    if err != nil {
        panic(err)
    }

    defer session.Close()

    // Optional. Switch the session to a monotonic behavior.
    session.SetMode(mgo.Monotonic, true)

    //convenient access
    c := session.DB("dummydata2").C("applications")

    // Query One
    result := Applications{}
    err = c.Find(bson.M{"name": "Dyned Live"}).One(&result)
    if err != nil {
        panic(err)
    }
    //fmt.Println("apns pem method ",result.APNS_PEM)
    return result.GCM_Access_Key
}

func ChangeStatus(Push_Code string) {
    session, err := mgo.Dial("localhost:27017")
    if err != nil {
        panic(err)
    }

    defer session.Close()

    // Optional. Switch the session to a monotonic behavior.
    session.SetMode(mgo.Monotonic, true)

    //convenient access
    c := session.DB("dummydata2").C("logs")
    
    //update
    condition := bson.M{"push_code": Push_Code}
    change := bson.M{"$set": bson.M{"status": "pushed"}}
    err = c.Update(condition, change)
    if err != nil {
        log.Fatal(err)
    }
}